package analyze.maven;

import analyze.Node;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PomResolverImplTest {

    @TempDir
    public Path tempDir;

    @Test
    void shouldBuildProjectTree() throws IOException {
        tempDir.resolve("pom.xml").toFile().createNewFile();

        File childDir = tempDir.resolve("child").toFile();
        childDir.mkdir();
        Path child1Pom = childDir.toPath().resolve("pom.xml");
        child1Pom.toFile().createNewFile();

        File child2Dir = tempDir.resolve("child2").toFile();
        child2Dir.mkdir();
        Path child2Pom = child2Dir.toPath().resolve("pom.xml");
        child2Pom.toFile().createNewFile();

        PomResolverImpl pomResolver = new PomResolverImpl();
        Node<Path> tree = pomResolver.getTree(tempDir);

        assertThat(tree.getData()).isRegularFile();
        assertThat(tree.getData()).hasFileName("pom.xml");
        assertThat(tree.getChildren()).containsOnly(new Node<>(child1Pom), new Node<>(child2Pom));
        assertThat(tree.getChildren().stream().findFirst().get().getParent()).isNotNull();
    }
}