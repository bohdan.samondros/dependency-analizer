package git;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

class RepositoryClonerImplTest {

    private RepositoryClonerImpl testedInstance = new RepositoryClonerImpl();

    @TempDir
    Path cloneTargetDir;

    @BeforeEach
    void setUp() {
        testedInstance.workingDir = cloneTargetDir.toString();
    }

    @Test
    void shouldCloneRepository() throws Exception {
        String repoUrl = "https://github.com/alecharp/simple-app.git";
        testedInstance.clone(repoUrl, "project-name");

        assertThat(cloneTargetDir.resolve("project-name")).exists();
    }
}