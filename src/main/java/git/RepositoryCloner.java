package git;

public interface RepositoryCloner {
    void clone(String remoteSource, String targetDir);
}
