package git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class RepositoryClonerImpl implements RepositoryCloner {
    private static final Logger log = LoggerFactory.getLogger(RepositoryClonerImpl.class);

    @Value(value = "${working.dir}")
    protected String workingDir;

    @Override
    public void clone(String remoteSource, String targetDir) {
        Path directory = Paths.get(workingDir, targetDir);
        directory.toFile().mkdir();
        try {
            Git call = Git.cloneRepository()
                    .setURI(remoteSource)
                    .setDirectory(directory.toFile())
                    .call();
            call.close();
        } catch (GitAPIException e) {
            log.error(e.getMessage(), e);
        }
    }
}
