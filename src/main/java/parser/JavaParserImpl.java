package parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class JavaParserImpl implements JavaParser {

    private Path javaFile;

    public JavaParserImpl(Path javaFile) {
        this.javaFile = javaFile;
    }

    @Override
    public List<String> resolveImports() {
        List<String> imports = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(javaFile.toFile()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("import")) {
                    String parsedClassName = line.replaceAll("[import]*[static]*\\s*\\t*;", "");
                    imports.add()
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String resolveCurrentPackage() {
        return null;
    }
}
