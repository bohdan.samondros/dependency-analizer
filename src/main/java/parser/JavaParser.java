package parser;

import java.util.List;

public interface JavaParser {
    List<String> resolveImports();
    String resolveCurrentPackage();
}
