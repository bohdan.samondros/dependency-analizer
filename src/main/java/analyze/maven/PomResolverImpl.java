package analyze.maven;

import analyze.Node;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;

public class PomResolverImpl implements PomResolver {

    @Override
    public Node<Path> getTree(Path dir) {
        Optional<Path> rootPom = getPom(dir);
        if (rootPom.isPresent()) {
            Path pom = rootPom.get();
            Node<Path> parentNode = new Node<>(pom);

            File[] childDirs = dir.toFile().listFiles(f -> f.isDirectory()
                    && !f.getName().equalsIgnoreCase("src")
                    && !f.getName().equalsIgnoreCase(".mvn")
            );
            for (File childDir : childDirs) {
                Node<Path> childNode = getTree(childDir.toPath());
                if (childNode != null) {
                    parentNode.getChildren().add(childNode);
                    childNode.setParent(parentNode);
                }
            }

            return parentNode;
        }
        return null;
    }

    private Optional<Path> getPom(Path projectRoot) {
        File[] files = projectRoot.toFile().listFiles();
        for (File file : files) {
            if (file.isFile() && file.getName().equalsIgnoreCase("pom.xml")) {
                return Optional.of(file.toPath());
            }
        }
        return Optional.empty();
    }
}
