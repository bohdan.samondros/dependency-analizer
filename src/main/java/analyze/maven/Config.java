package analyze.maven;

public enum  Config {
    JAVA_SOURCES("src/main/java"),
    JAVA_TEST_SOURCES("src/test/java");

    private String path;

    Config(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
