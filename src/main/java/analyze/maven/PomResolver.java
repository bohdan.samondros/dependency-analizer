package analyze.maven;

import analyze.Node;

import java.nio.file.Path;
import java.util.Optional;

public interface PomResolver {
    Node<Path> getTree(Path dir);
}
